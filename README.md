# Hello World ECS Task

Terraform ECS Deployment for Hello World node.js app

## Architecture
![Hello World Architecture](/assets/helloworld-task-irs.drawio.png)

## Procedures
1. Clone Git repository to get the files
```
git clone git@gitlab.com:ragbay/helloworld-task-irs.git
cd helloworld-task-irs
```
2. Update provide.tf for your AWS credentials
3. Update terraform.tfvars with the appropriate values
4. Deploy using terraform commands
```
terraform init
terraform plan
terraform apply
```
## Files

### terraform.tfvars
- Input variable definitions

### variables.tf
- Variable declaration

### provider.tf
- Terraform aws provider for AWS credentials and region
- Indicate your own key here

### vpc.tf
 - VPC Resources:
 - CIDR Block: 10.1.0.0/16
 - 3 Availability Zones; 6 subnets; 3 public and 3 private subnets; One private and public subnets for each AZ
 - Data block to get available AZ in the AWS account/region
 - NAT Gateway resource will allow resources to communicate within VPC
 - Internet Gateway resource to allow resources to communicate between vpc and internet

### iam.tf
 - IAM Role needed to execute ECS Tasks
 - IAM Role for lambda web checker

### cloudwatch.tf
 - ECS events monitoring for stopped tasks 
 - URL Checker Lambda Scheduler

### lambda.tf
 - lambda function for URL Checker

### sns.tf
 - sns topic and subscriber for cloudwatch alerts

### ecs-app-alb.tf
 - Internet facing Application Load Balancer to distribute traffic across the web application tasks
 - ALB has a listener/target group to forward port 80 traffic to ECS
 - ALB security group will only allow port 80 traffic from sources anywhere. 

### ecs-app-task-definition.tf
 - Defined to use FARGATE(AWS managed ECS host) with specified cpu/memory allocation.  
 - Container image pulled from Docker Hub using sample Hello World heroku node.js container image
 - The Docker container is exposed on port 3000
 - Network mode is set to "awsvpc", which will have an elastic network interface and a private IP address assigned to the task when it runs

### ecs-app-cluster.tf
 - ECS cluster resource definition for the app

### ecs-app-service.tf
 - ECS service specifies the number of tasks the app should be run with the task_definition and desired_count properties within the cluster
 - ECS service dependency on the ALB has been configured to make sure the ALB exists first
 - Security group created for ECS service, to allow only traffic from load balancer over tcp port 3000

### output.tf
 - Added output to show the Load Balancer URL at the end of terraform execution.

### python/lambda_function.py
 - lambda python code to check on the availability of the site every 1 minute
