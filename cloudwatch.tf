resource "aws_cloudwatch_event_rule" "helloworld_task_ecs_event_rule" {
  name        = "helloworld-task-ecs-event-rule"
  description = "Capture ECS events"

  event_pattern = <<EOF
{
   "source":[
      "aws.ecs"
   ],
   "detail-type":[
      "ECS Task State Change"
   ],
   "detail":{
      "lastStatus":[
         "STOPPED"
      ]
   }
}
EOF
}

resource "aws_cloudwatch_event_target" "helloworld_task_sns_target" {
  rule      = aws_cloudwatch_event_rule.helloworld_task_ecs_event_rule.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.helloworld_task_sns_topic.arn
}


resource "aws_cloudwatch_event_rule" "helloworld_task_python_event_rule" {
  name                = "helloworld-task-python-event-rule"
  description         = "Scheduled checking of Hello World"
  schedule_expression = "rate(1 minute)"
}

resource "aws_cloudwatch_event_target" "helloworld_task_python_event_target" {
  target_id = "helloworld-task-python-event-target"
  rule      = aws_cloudwatch_event_rule.helloworld_task_python_event_rule.name
  arn       = aws_lambda_function.helloworld_task_lambda_function.arn
}

resource "aws_lambda_permission" "helloworld_task_allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.helloworld_task_lambda_function.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.helloworld_task_python_event_rule.arn
}