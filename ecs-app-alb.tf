resource "aws_lb" "helloworld_task_alb" {
  name            = "helloworld-task-alb"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.lb.id]
}

resource "aws_lb_target_group" "helloworld_task_tg" {
  name        = "helloworld-task-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.helloworld_task_vpc.id
  target_type = "ip"
}

resource "aws_lb_listener" "helloworld_task_listener" {
  load_balancer_arn = aws_lb.helloworld_task_alb.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.helloworld_task_tg.id
    type             = "forward"
  }
}

resource "aws_security_group" "lb" {
  name   = "helloworld-task-alb-security-group"
  vpc_id = aws_vpc.helloworld_task_vpc.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  # If https support is needed, include this
  #   ingress {
  #     protocol    = "tcp"
  #     from_port   = 443
  #     to_port     = 443
  #     cidr_blocks = ["0.0.0.0/0"]
  #   }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
