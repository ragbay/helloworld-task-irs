resource "aws_ecs_service" "helloworld_task_service" {
  name            = "helloworld-task-service"
  cluster         = aws_ecs_cluster.helloworld_task_cluster_app.id
  task_definition = aws_ecs_task_definition.helloworld_task_taskdef.arn
  desired_count   = 3
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.task_sg.id]
    subnets         = aws_subnet.private.*.id
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.helloworld_task_tg.id
    container_name   = "helloworld-task-app"
    container_port   = var.ecs_task_port
  }

  depends_on = [aws_lb_listener.helloworld_task_listener]
}

resource "aws_security_group" "task_sg" {
  name   = "helloworld-task-task-sg"
  vpc_id = aws_vpc.helloworld_task_vpc.id

  ingress {
    protocol        = "tcp"
    from_port       = var.ecs_task_port
    to_port         = var.ecs_task_port
    security_groups = [aws_security_group.lb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
