data "aws_iam_role" "ecs_task_execution_role" {
  name = aws_iam_role.ecs_task_exec_role.name
}

resource "aws_ecs_task_definition" "helloworld_task_taskdef" {
  family                   = "helloworld-task-app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 2048
  execution_role_arn       = data.aws_iam_role.ecs_task_execution_role.arn

  container_definitions = <<DEFINITION
[
  {
    "image": "heroku/nodejs-hello-world",
    "cpu": 1024,
    "memory": 2048,
    "name": "helloworld-task-app",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": ${jsonencode(var.ecs_task_port)},
        "hostPort": ${jsonencode(var.ecs_task_port)}
      }
    ]
  }
]
DEFINITION
}
