
resource "aws_iam_role" "ecs_task_exec_role" {
  name               = "helloworld_task-ecsTaskExecutionRole"
  description        = "Managed by Terraform"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_exec_role_policy_document.json
}

data "aws_iam_policy_document" "ecs_task_exec_role_policy_document" {
  statement {
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "AmazonECSTaskExecutionRolePolicy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs-task-exec-role-policy-attach" {
  role       = aws_iam_role.ecs_task_exec_role.name
  policy_arn = data.aws_iam_policy.AmazonECSTaskExecutionRolePolicy.arn
}


resource "aws_iam_role" "lambda_exec_role" {
  name               = "helloworld_task-lambdaExecutionRole"
  description        = "Managed by Terraform"
  assume_role_policy = data.aws_iam_policy_document.lambda_exec_role_policy_document.json
}

data "aws_iam_policy_document" "lambda_exec_role_policy_document" {
  statement {
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "AWSLambdaBasicExecutionRole" {
  arn = "arn:aws:iam::aws:policy/AWSLambdaExecute"
}

data "aws_iam_policy" "AmazonSNSFullAccess" {
  arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}


resource "aws_iam_role_policy_attachment" "lambda-exec-role-policy-attach" {
  role       = aws_iam_role.lambda_exec_role.name
  policy_arn = data.aws_iam_policy.AWSLambdaBasicExecutionRole.arn
}


resource "aws_iam_role_policy_attachment" "lambda-sns-exec-role-policy-attach" {
  role       = aws_iam_role.lambda_exec_role.name
  policy_arn = data.aws_iam_policy.AmazonSNSFullAccess.arn
}