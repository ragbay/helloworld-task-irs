data "archive_file" "helloworld_task_python_lambda_package" {
  type        = "zip"
  source_file = "./python/lambda_function.py"
  output_path = "lambda_web_test.zip"
}

resource "aws_lambda_function" "helloworld_task_lambda_function" {
  function_name    = "helloworld-task-lamba-web-test"
  filename         = "lambda_web_test.zip"
  source_code_hash = data.archive_file.helloworld_task_python_lambda_package.output_base64sha256
  role             = aws_iam_role.lambda_exec_role.arn
  runtime          = "python3.8"
  handler          = "lambda_function.lambda_handler"
  timeout          = 300
  environment {
    variables = {
      ELB_URL = "http://${aws_lb.helloworld_task_alb.dns_name}",
      SNS_ARN = aws_sns_topic.helloworld_task_sns_topic.arn
    }
  }
}