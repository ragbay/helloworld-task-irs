import boto3
import json
import os
from urllib.request import urlopen
from urllib.error import *

def lambda_handler(event, context):
    client = boto3.client('sns')
    
    try:
        html = urlopen(os.environ['ELB_URL'])
    except HTTPError as e:
        notification = "HTTP Error: " + str(e)
        client = boto3.client('sns')
        response = client.publish (
          TargetArn = os.environ['SNS_ARN'],
          Message = json.dumps({'default': notification}),
          MessageStructure = 'json'
        )
    except URLError as e:
        notification = "URL Error: " + str(e)
        client = boto3.client('sns')
        response = client.publish (
          TargetArn = os.environ['SNS_ARN'],
          Message = json.dumps({'default': notification}),
          MessageStructure = 'json'
        )
    else:
        print('OK 200 ')