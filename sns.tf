resource "aws_sns_topic" "helloworld_task_sns_topic" {
  name = "helloworld-task-sns-topic"
}

resource "aws_sns_topic_subscription" "helloworld_task_sns_topic_sub_email" {
  count     = length(var.ecs_sns_emails)
  topic_arn = aws_sns_topic.helloworld_task_sns_topic.arn
  protocol  = "email"
  endpoint  = var.ecs_sns_emails[count.index]
}
