variable "aws_region" {
  description = "The AWS region that contains managed infrastructure."
  default     = "us-east-1"
}

variable "vpc_cidr_block" {
  description = "The CIDR block for the managed VPC."
}

variable "ecs_task_port" {
  description = "The port number for the task definition"
}

variable "ecs_sns_emails" {
  type        = list(string)
  description = "List of email SNS Notification for ECS."
}